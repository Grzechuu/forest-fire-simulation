export const hasArrayIndex = (array, index): boolean => {
  return Array.isArray(array) && array.hasOwnProperty(index);
};

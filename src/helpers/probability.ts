export const startFireProbability = (probability = 0.5): boolean => {
  return Math.random() < probability;
};

export const generateTileType = () => {
  let d = Math.random() * 100;
  if ((d -= 60) < 0) return 'forest';
  if ((d -= 40) < 0) return 'water';
};

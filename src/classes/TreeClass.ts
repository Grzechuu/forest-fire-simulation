import Water from './WaterClass';

// import { BoardItem } from './Board';

class Tree {
  isBurning: boolean;
  isBurnt: boolean;
  canBurn: boolean;
  neighbours: any[];
  position: {
    row: number;
    index: number;
  };
  startBurningItteration: null | number;
  constructor(row, index) {
    this.isBurning = false;
    this.isBurnt = false;
    this.canBurn = true;
    this.position = { row, index };
    this.neighbours = [];
    this.startBurningItteration = null;
  }

  get isAlive(): boolean {
    return !this.isBurning && !this.isBurnt;
  }

  probability = (): boolean => {
    let d = Math.random() * 100;
    if ((d -= 50) < 0) return false;
    if ((d -= 50) < 0) return true;
  };

  isBurnable(): boolean {
    return this.canBurn;
  }

  setNeighbours(neighbours): void {
    this.neighbours = neighbours;
  }
  isTreeBurning(): boolean {
    return this.isBurning;
  }

  startBurning(): void {
    this.isBurning = true;
  }

  becomeBurnt(): void {
    this.isBurning = false;
    this.isBurnt = true;
  }
  becomeAlive(): void {
    this.isBurning = false;
    this.isBurnt = false;
    this.startBurningItteration = 0;
  }

  shouldAutoIgnite(counter: number): void {
    if (Math.random() > 0.99999) {
      this.startBurning();
      this.startBurningItteration = counter;
    }
  }

  shouldBecomeAlive(value: number): void {
    if (
      this.setStartBurningItteration !== null &&
      value - this.startBurningItteration >= 3 &&
      Math.random() <
        0.005 * this.neighbours.filter((item: any) => item instanceof Tree && item.isAlive).length +
          0.05 * this.neighbours.filter((item: any) => item instanceof Water).length
    ) {
      this.becomeAlive();
    }
  }

  shouldBeBurnt(value: number): void {
    if (this.setStartBurningItteration !== null && value - this.startBurningItteration >= 1) {
      this.becomeBurnt();
    }
  }

  setStartBurningItteration(value): void {
    this.startBurningItteration = value;
  }
}

export default Tree;

import Water from './WaterClass';
import Tree from './TreeClass';
import { hasArrayIndex } from '../helpers/Array';
import { startFireProbability } from '../helpers/probability';
import Heightmap from '../heightmap';

const itterationTime = 500;

const size = 6;
// 2^n + 1
const gridSize = 2 ** size + 1;

export type BoardItem = Water | Tree;
class Board {
  isSimulationRunning: boolean;
  board: Array<BoardItem>[];
  counter: number;
  setCounter: (x: number) => void;
  graphData: any[];
  selfIgniteAllowed: boolean;
  becomingAliveAllowed: boolean;
  fireProbability: number;
  constructor(counter, setCounter) {
    this.isSimulationRunning = false;
    this.board = [];
    this.counter = counter;
    this.setCounter = setCounter;
    this.graphData = [];
    this.selfIgniteAllowed = false;
    this.becomingAliveAllowed = false;
    this.fireProbability = 0.5;
  }

  setInitials(): void {
    this.counter = 0;
    this.board = [];
    this.graphData = [];
  }

  public set probability(v: number) {
    this.fireProbability = Math.min(Math.max(0, v), 1);
  }

  public set selfIgnite(v: boolean) {
    this.selfIgniteAllowed = v;
  }
  public set becomeAlive(v: boolean) {
    this.becomingAliveAllowed = v;
  }

  gridElementsNeighbours(): void {
    this.board &&
      this.board.map((row, rowIndex) => {
        row.map((item, itemIndex) => {
          const neighbours = [];

          //TOP-RIGHT
          hasArrayIndex(this.board[rowIndex - 1], itemIndex + 1) &&
            neighbours.push(this.board[rowIndex - 1][itemIndex + 1]);
          // //TOP
          hasArrayIndex(this.board[rowIndex - 1], itemIndex) && neighbours.push(this.board[rowIndex - 1][itemIndex]);
          //TOP LEFT
          hasArrayIndex(this.board[rowIndex - 1], itemIndex - 1) &&
            neighbours.push(this.board[rowIndex - 1][itemIndex - 1]);
          // //LEFT
          hasArrayIndex(row, itemIndex - 1) && neighbours.push(row[itemIndex - 1]);
          // //RIGHT
          hasArrayIndex(row, itemIndex + 1) && neighbours.push(row[itemIndex + 1]);
          // //BOTTOM
          hasArrayIndex(this.board[rowIndex + 1], itemIndex) && neighbours.push(this.board[rowIndex + 1][itemIndex]);
          // //BOTTOM RIGHT
          hasArrayIndex(this.board[rowIndex + 1], itemIndex + 1) &&
            neighbours.push(this.board[rowIndex + 1][itemIndex + 1]);
          // //BOTTOM LEFT
          hasArrayIndex(this.board[rowIndex + 1], itemIndex - 1) &&
            neighbours.push(this.board[rowIndex + 1][itemIndex - 1]);

          const currentItem = this.board[rowIndex][itemIndex];
          if (currentItem instanceof Tree) {
            currentItem.setNeighbours(neighbours);
          }
        });
      });
  }

  generateGrid(): void {
    this.stopSimulation();
    this.setInitials();
    const board = [];
    const heightMap = Heightmap.create(size);

    for (let i = 1; i < gridSize - 1; i++) {
      const rowArray = [];
      for (let j = 1; j < gridSize - 1; j++) {
        if (
          1 * heightMap[i - 1][j - 1] +
            2 * heightMap[i - 1][j] +
            1 * heightMap[i - 1][j + 1] +
            2 * heightMap[i][j - 1] +
            4 * heightMap[i][j] +
            2 * heightMap[i][j + 1] +
            1 * heightMap[i + 1][j - 1] +
            2 * heightMap[i + 1][j] +
            1 * heightMap[i + 1][j + 1] >
          16 * 0.55
        ) {
          rowArray.push(new Tree(i, j));
        } else {
          rowArray.push(new Water(i, j));
        }
      }
      board.push(rowArray);
    }

    this.board = board;
    this.gridElementsNeighbours();
    this.generateGraphData();
  }

  updateCounter(): void {
    this.setCounter(this.counter + 1);
    this.counter = this.counter + 1;
  }

  updateBurntTrees(): void {
    if (this.selfIgniteAllowed) {
      this.allGreenTrees().map((item) => {
        item.shouldAutoIgnite(this.counter);
      });
    }

    if (this.becomingAliveAllowed) {
      this.allBurntTrees().map((item) => {
        item.shouldBecomeAlive(this.counter);
      });
    }

    this.allBurningTrees().map((item) => {
      item.shouldBeBurnt(this.counter);
    });
  }

  stopSimulation() {
    this.isSimulationRunning = false;
  }

  updateBoard(): void {
    if (this.isSimulationRunning) {
      if (
        (this.greenTreesLeft() && this.allBurningTrees().length > 0) ||
        (this.selfIgniteAllowed && this.becomingAliveAllowed)
      ) {
        this.updateCounter();
        this.continueSimulation();
        this.generateGraphData();
      } else {
        //LAST UPDATE
        this.updateCounter();
        setTimeout(() => {
          this.updateBurntTrees();
          this.generateGraphData();
          this.updateCounter();
          this.stopSimulation();
        }, itterationTime);
      }
    } else {
      return;
    }
  }

  startSimulation(): void {
    const greenTrees = this.allGreenTrees();
    const obj = greenTrees[Math.floor(Math.random() * greenTrees.length)];

    if (obj.isAlive && this.allBurningTrees().length === 0) {
      this.isSimulationRunning = true;
      obj.startBurning();
      this.updateBoard();
    } else if (obj.isAlive && this.allBurningTrees().length > 0) {
      // WHEN FIRE IS ALREADY SPRADING AND COUNTER IS COUNTING ITTERATIONS
      //WILL ADD NEW FIRE SOUCE IN NEXT ITTERATION
      obj.startBurning();
    } else {
      this.startSimulation();
    }
  }

  allGreenTrees(): Tree[] {
    const tempArr = [];
    this.board.map((row) => {
      return row.map((item) => {
        if (item instanceof Tree && !item.isBurning && !item.isBurnt) {
          tempArr.push(item);
        }
      });
    });
    return tempArr;
  }

  allBurntTrees(): Tree[] {
    const tempArr = [];
    this.board.map((row) => {
      return row.map((item) => {
        if (item instanceof Tree && !item.isBurning && item.isBurnt) {
          tempArr.push(item);
        }
      });
    });
    return tempArr;
  }

  allBurningTrees(): Tree[] {
    const tempArr = [];
    this.board.map((row) => {
      return row.map((item) => {
        if (item instanceof Tree && item.isBurning && !item.isBurnt) {
          tempArr.push(item);
        }
      });
    });
    return tempArr;
  }

  greenTreesLeft(): boolean {
    const tempArr = [];
    this.board.map((row) => {
      return row.map((item) => {
        if (item instanceof Tree && !item.isBurning && !item.isBurnt) {
          tempArr.push(item);
        }
      });
    });
    return tempArr.length > 0;
  }

  continueSimulation(): void {
    const value = this.counter;
    setTimeout(() => {
      const burningTrees = this.allBurningTrees();
      burningTrees.map((burningTree) => {
        burningTree.neighbours.map((neighbour: BoardItem) => {
          if (neighbour instanceof Tree && startFireProbability(this.fireProbability) && neighbour.isAlive) {
            neighbour.startBurning();
            neighbour.setStartBurningItteration(value);
          }
        });
      });

      this.updateBurntTrees();
      this.updateBoard();
    }, itterationTime);
  }

  generateGraphData(): void {
    // const burningTrees = this.allBurningTrees().length;
    // const burntTrees = this.allBurntTrees().length;
    // const greenTrees = this.allGreenTrees().length;
    const data = {
      name: this.counter,
      żywe: this.allGreenTrees().length,
      płonące: this.allBurningTrees().length,
      spalone: this.allBurntTrees().length,
    };
    this.graphData = [...this.graphData, data];
  }
}

export default Board;

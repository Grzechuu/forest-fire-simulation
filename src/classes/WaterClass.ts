class Water {
  canBurn: boolean;
  position: {
    row: number;
    index: number;
  };
  constructor(row, index) {
    this.canBurn = false;
    this.position = { row, index };
  }
}

export default Water;

import * as React from 'react';
import cx from 'classnames';

const Water: React.FC = () => {
  return <div className={cx('tile water')}></div>;
};

export default React.memo(Water);

import * as React from 'react';
import cx from 'classnames';

const BurningTree: React.FC = () => {
  return <div className={cx('tile forest-burning')}></div>;
};

export default React.memo(BurningTree);

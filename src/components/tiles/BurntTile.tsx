import * as React from 'react';
import cx from 'classnames';

const BurntTile: React.FC = () => {
  return <div className={cx('tile forest-burnt')}></div>;
};

export default React.memo(BurntTile);

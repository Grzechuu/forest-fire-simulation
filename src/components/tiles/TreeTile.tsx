import * as React from 'react';
import cx from 'classnames';

const Tree: React.FC = () => {
  return <div className={cx('tile forest')}></div>;
};

export default React.memo(Tree);

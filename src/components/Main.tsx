import * as React from 'react';
import WaterTile from './tiles/WaterTile';
import * as uuid from 'uuid';
import TreeTile from './tiles/TreeTile';
import Tree from '../classes/TreeClass';
import Water from '../classes/WaterClass';
import Board from '../classes/Board';
import BurningTree from './tiles/BurningTile';
import BurntTree from './tiles/BurntTile';
import { useForceUpdate } from '../hooks/useForceUpdate';

const { LineChart, Line, XAxis, YAxis, Tooltip, CartesianGrid, Legend } = Recharts;

const { useState, useEffect, useCallback } = React;

const Main: React.FC = () => {
  const [grid, setGrid] = useState([]);
  const [counter, setCounter] = useState(0);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const board = React.useMemo(() => new Board(counter, setCounter), []);

  const forceUpdate = useForceUpdate();

  const handleBecomingAliveRuleChange = (): void => {
    board.becomeAlive = !board.becomingAliveAllowed;
    forceUpdate();
  };

  const handleSelfIgniteRule = (): void => {
    board.selfIgnite = !board.selfIgniteAllowed;
    forceUpdate();
  };

  const startFire = useCallback(() => {
    board.startSimulation();
  }, [board]);

  const renderGrid = useCallback(
    (grid) => {
      return (
        <div className="wrapper">
          {grid &&
            grid.map((row, index) => {
              return (
                <div key={index} className="row">
                  {row.map((tile, tileIndex) => {
                    if (tile instanceof Water) {
                      return <WaterTile key={`${index}.${tileIndex}`} />;
                    } else if (tile instanceof Tree) {
                      if (tile.isBurnt) {
                        return <BurntTree key={`${index}.${tileIndex}`} />;
                      }
                      if (tile.isBurning) {
                        return <BurningTree key={`${index}.${tileIndex}`} />;
                      } else {
                        return <TreeTile key={`${index}.${tileIndex}`} />;
                      }
                    }
                  })}
                </div>
              );
            })}
          <button onClick={(): void => startFire()}>START</button>
        </div>
      );
    },
    [startFire]
  );

  useEffect(() => {
    board.generateGrid();
  }, [board]);

  useEffect(() => {
    setGrid(board.board);
  }, [board, board.board, counter]);

  const handleNewMap = (): void => {
    board.generateGrid();
    setGrid(board.board);
  };

  const handleProbabilityChange = (e) => {
    board.probability = e.target.value / 100;

    forceUpdate();
  };

  return (
    <div className="wrapper">
      {renderGrid(grid)}
      <button onClick={(): void => handleNewMap()}>Nowa mapa</button>
      <label htmlFor="reborn">Odradzanie</label>
      <input
        type="checkbox"
        onChange={(): void => handleBecomingAliveRuleChange()}
        checked={board.becomingAliveAllowed}
        id="reborn"
      />
      <label htmlFor="selfIgnite">Autozapłon</label>
      <input
        type="checkbox"
        onChange={(): void => handleSelfIgniteRule()}
        checked={board.selfIgniteAllowed}
        id="selfIgnite"
      />
      <span>Prawdopodobieństwo podpalenia sąsiedniego drzewa</span>
      <span className="valuePadding input-holder">
        <input
          type="number"
          max="100"
          min="0"
          step="1"
          id="inputMarkUp"
          value={board.fireProbability * 100}
          onChange={(e): void => handleProbabilityChange(e)}
        />
      </span>
      {/* <input
        type="number"
        onChange={(e): void => handleProbabilityChange(e)}
        className="input-holder"
        value={board.fireProbability * 100}
        min="0"
        max="100"
        step="1"
      /> */}
      <LineChart data={board.graphData} width={600} height={400} margin={{ top: 50, bottom: 50, left: 50, right: 50 }}>
        <XAxis dataKey="name" type="number" allowDecimals={false} />
        <YAxis />
        <Tooltip />
        <CartesianGrid strokeDasharray="3 3" />
        <Legend />
        <Line
          type="monotone"
          dataKey="żywe"
          stroke="#00ff00"
          dot={{ stroke: '#00ff00', strokeWidth: '0.5rem', r: 0 }}
        />
        <Line
          type="monotone"
          dataKey="płonące"
          stroke="#ff0000"
          dot={{ stroke: '#ff0000', strokeWidth: '0.5rem', r: 0 }}
        />
        <Line
          type="monotone"
          dataKey="spalone"
          stroke="#000000"
          dot={{ stroke: '#000000', strokeWidth: '0.5rem', r: 0 }}
        />
      </LineChart>
    </div>
  );
};
export default Main;

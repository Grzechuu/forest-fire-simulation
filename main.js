const { app, BrowserWindow, Menu } = require('electron');
const url = require('url');
const path = require('path');
// const extension = require('electron-devtools-installer');

// // const { default: installExtension, REACT_DEVELOPER_TOOLS } = require('electron-devtools-installer');

// // installExtension(REACT_DEVELOPER_TOOLS);

// require('electron-reload')(__dirname);

let mainWindow;
let addWindow;

const appTemplatePath = `file://${__dirname}/index.html`;

// function createAddWindow() {
//   addWindow = new BrowserWindow({
//     width: 1920,
//     height: 1080,
//     title: 'Add Shopping List Item',
//   });
//   addWindow.loadURL(
//     url.format({
//       pathname: path.join(__dirname, 'addWindow.html'),
//       protocol: 'file:',
//       slashes: true,
//     })
//   );
//   // Handle garbage collection
//   addWindow.on('close', function() {
//     addWindow = null;
//   });
// }

const mainMenuTemplate = [
  // Each object is a dropdown
  {
    label: 'File',
    submenu: [
      {
        label: 'Quit',
        accelerator: process.platform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
        click() {
          app.quit();
        },
      },
    ],
  },
];

function createWindow() {
  mainWindow = new BrowserWindow({
    width: 1000,
    height: 1200,
    // webPreferences: {
    //     nodeIntegration: true,
    //     preload: path.join(__dirname, 'preload.js')
    // }
  });
  mainWindow.loadURL(appTemplatePath);
  mainWindow.once('ready-to-show', () => {
    mainWindow.show();
  });
  // mainWindow.loadFile('index.html');
  mainWindow.on('closed', () => {
    mainWindow = null;
  });
  const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
  // Insert menu
  Menu.setApplicationMenu(mainMenu);
}

app.on('ready', createWindow);

app.on('window-all-closed', function() {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit();
});

app.on('activate', function() {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) createWindow();
});

// if ((process.env.NODE_ENV || '').trim() !== 'prod') {
if (!app.isPackaged) {
  mainMenuTemplate.push({
    label: 'Developer Tools',
    submenu: [
      {
        role: 'reload',
      },
      {
        label: 'Toggle DevTools',
        accelerator: process.platform == 'darwin' ? 'Command+I' : 'Ctrl+I',
        click(item, focusedWindow) {
          focusedWindow.toggleDevTools();
        },
      },
    ],
  });
}
